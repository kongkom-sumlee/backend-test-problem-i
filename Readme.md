# How to run project in local environment (calculator api)

##### 1. open project file

- open terminal in project file

##### 2. install package json

- ### `npm install`

##### 3. run project

- ### `npm run dev`

##### 4. call api with postman 

- curl --location --request GET 'http://localhost:3000/problem/v1/one' \
--header 'Authorization: AAAA' \
--header 'Content-Type: application/json' \
--data '{
    "a": 10,
    "b": 6
}'

- curl --location --request GET 'http://localhost:3000/problem/v1/one' \
--header 'Authorization: DEVCREW-BACKEND-TEST' \
--header 'Content-Type: application/json' \
--data '{
    "a": 10,
    "b": 6
}'

- curl --location --request GET 'http://localhost:3000/problem/v1/one' \
--header 'Authorization: DEVCREW-BACKEND-TEST' \
--header 'Content-Type: application/json' \
--data '{
    "a": -2,
    "b": 5.1
}'

- curl --location --request GET 'http://localhost:3000/problem/v1/one' \
--header 'Authorization: DEVCREW-BACKEND-TEST' \
--header 'Content-Type: application/json' \
--data '{
    "a": 10
}'

##### credit by Kongkom Sumlee
