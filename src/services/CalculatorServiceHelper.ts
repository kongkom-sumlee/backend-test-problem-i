import ResultResponse from '../interfaces/response/ResultResponse.interface'

class CalculatorServiceHelper {
    public addNumber(numberA: number, numberB: number): number {
        return numberA + numberB
    }

    public subtractNumber(numberA: number, numberB: number): number {
        return numberA - numberB
    }

    public multiplyNumber(numberA: number, numberB: number): number {
        return numberA * numberB
    }

    public divideNumber(numberA: number, numberB: number): number {
        return numberA / numberB
    }

    public mapResultResponse(result: number): ResultResponse {
        return {
            result: result
        }
    }
}

export default new CalculatorServiceHelper()
