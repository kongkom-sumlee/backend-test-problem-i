import helper from './CalculatorServiceHelper'
import MultiplyNumberRequest from '../interfaces/request/MultiplyNumberRequest.interface'
import validator from '../utilities/Validator'
import * as schemaType from '../enums/SchemaType'

class CalculatorService {
    public async multiplyNumber(multiplyNumberRequest: MultiplyNumberRequest, headers: any) {
        validator.validateAuthorization(headers)
        const validateRequestResult = validator.validate(multiplyNumberRequest, schemaType.type.VALIDATE_MULTIPLY_NUMBER_REQUEST_SCHEMA)

        validator.handleValidateResult(validateRequestResult)

        const numberA: number = multiplyNumberRequest.a
        const numberB: number = multiplyNumberRequest.b

        const result = helper.multiplyNumber(numberA, numberB)
        const response = helper.mapResultResponse(result) 

        return response
    }

}

export default new CalculatorService()
