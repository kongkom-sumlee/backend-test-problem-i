import express, { Request, Response, Router } from 'express'
import calculatorService from '../services/CalculatorService'
import CustomError from '../utilities/CustomError'
import { generateErrorResponse } from '../utilities/ResponseHandler'

const router: Router = express.Router()

router.get('/v1/one', async(req: Request, res: Response) => {
    try {
        const response = await calculatorService.multiplyNumber(req.body, req.headers)

        res.status(200).json(response)
    } catch (err) {
        if (err instanceof CustomError) {
          const errorResponse = generateErrorResponse(err)
          res.status(err.statusCode).json(errorResponse)
        } else {
            res.status(500).json(generateErrorResponse('Internal server error'))
        }
    }
})

export default router
