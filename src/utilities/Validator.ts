import CustomError from './CustomError'
import * as schema from './Schema'
import * as schemaType from '../enums/SchemaType'

const SCHEMA_LIST: any = {
  [schemaType.type.VALIDATE_MULTIPLY_NUMBER_REQUEST_SCHEMA]: schema.VALIDATE_MULTIPLY_NUMBER_REQUEST_SCHEMA,
}

const options = {
  abortEarly: false
}

function validate(value: object, type: schemaType.type) {
  const result = SCHEMA_LIST[type].validate(value, options)

  return result
}

function validateAuthorization(headers: any) {
    if (!headers || !headers.authorization || headers.authorization != 'DEVCREW-BACKEND-TEST') throw new CustomError(401, 'Unauthorized')
}

function handleValidateResult(validateResult: any) {
    if (validateResult.error) throw new CustomError(422, 'Unsupported data format')
    
}

function validateFile(requestFile: any) {
    if (!requestFile) throw new CustomError(422, 'Please select a file')
}

export default {
    validate, validateAuthorization, handleValidateResult, validateFile
}
