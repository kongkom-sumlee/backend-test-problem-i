import Joi from 'joi'

export const VALIDATE_MULTIPLY_NUMBER_REQUEST_SCHEMA: Joi.ObjectSchema<any> = Joi.object({
    a: Joi.number().strict().required(),
    b: Joi.number().strict().required()
  }).unknown(true)
